﻿namespace FileSystemAnalysis
{
    class ExtGrp
    {
        private int count = 0;
        private long totalLength = 0;
        private long avgLength = 0;

        public ExtGrp(int count, long totalLength, long avgLength)
        {
            this.count = count;
            this.totalLength = totalLength;
            this.avgLength = avgLength;
        }

        public int Count
        {
            get { return this.count; }
        }

        public long TotalLength
        {
            get { return this.totalLength; }
        }

        public long AvgLength
        {
            get { return this.avgLength; }
        }
    }
}
