﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FileSystemAnalysis
{
    class FileSystemAnalysis
    {
        public static event EventHandler<FSAnalysisEventArgs> UnaccessiblePathReached;
        public static event EventHandler<FSAnalysisEventArgs> ScanCompleted;
        public static event EventHandler<FSAnalysisEventArgs> GenerateReportStarted;
        public static event EventHandler<FSAnalysisEventArgs> GenerateReportFinished;
        public static event EventHandler<FSAnalysisEventArgs> ScanNewDirStarted;
        public static event EventHandler<FSAnalysisEventArgs> ExecutionStopped;
        private FSAnalysisEventArgs fsa;
        private string path;
        private readonly int ID;
        private string filename = @"../../../report";
        private readonly string ext = ".txt";
        private StringBuilder sb = new StringBuilder();
        private bool stopped = false;

        private enum Events
        {
            Unaccess,
            ScanComplete,
            GenReportStart,
            GenReportFinish,
            ScanNewDir,
            Stop
        }

        public FileSystemAnalysis()
        {
            
        }

        public FileSystemAnalysis(string path, string name, int id)
        {
            this.path = path;
            this.filename = name;
            this.ID = id;
            fsa = new FSAnalysisEventArgs();
        }

        public FileSystemAnalysis(string path, int id)
        {
            this.path = path;
            this.ID = id;
            this.filename = this.filename + this.ID + this.ext;
            fsa = new FSAnalysisEventArgs();
        }

        public void Start()
        {
            if (Directory.Exists(@path) && !String.IsNullOrEmpty(@path))
            {
                DirectoryInfo di = new DirectoryInfo(@path);
                ListAllFiles(di, "*");

                if (stopped)
                {
                    Terminate();
                    return;
                }

                RiseNewEvent("Scan complete", Events.ScanComplete);

                if (stopped)
                {
                    RiseNewEvent("Thread stopped before report", Events.Stop);
                    Terminate();
                    return;
                }

                RiseNewEvent("Start generating report", Events.GenReportStart);

                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
                File.AppendAllText(filename, sb.ToString());

                RiseNewEvent("Report generated", Events.GenReportFinish);
                Terminate();
            }
        }

        public bool Stopped
        {
            get { return this.stopped; }
            set { this.stopped = value; }
        }

        private void Terminate()
        {
            FileSystemInfo.RemoveThread(ID);
        }

        //recursive get data from dir
        private void ListAllFiles(DirectoryInfo di, string searchPattern)
        {
            if (stopped)
            {
                RiseNewEvent("Thread stopped while scan", Events.Stop);
                Terminate();
                return;
            }

            RiseNewEvent("Scaning new directory " + di.FullName, Events.ScanNewDir);
            try
            {
                List<string> exts = new List<string>();
                Dictionary<string, ExtGrp> grpsbyext = new Dictionary<string,ExtGrp>();
                int subDirCount = di.GetDirectories().Length;
                foreach (FileInfo f in di.GetFiles(searchPattern))
                {
                    if (!String.IsNullOrEmpty(f.Extension))
                    {
                        exts.Add(f.Extension);
                    }
                }
                string[] fileExt = exts.Select(f => Path.GetExtension(f)).Distinct().OrderBy(f => f).ToArray();

                foreach (string ext in fileExt)
                {
                    if (stopped)
                    {
                        RiseNewEvent("Thread stopped while analyzing groups", Events.Stop);
                        Terminate();
                        return;
                    }

                    FileInfo[] grp = di.GetFiles("*" + ext);
                    long totalGrpSize = 0;
                    int grpCount = grp.Length;
                    foreach (FileInfo fe in grp)
                    {
                        totalGrpSize += fe.Length;  //total size of grp
                    }
                    long avSize = totalGrpSize / grpCount;

                    ExtGrp eg = new ExtGrp(grpCount, totalGrpSize, avSize);
                    grpsbyext.Add(ext, eg);
                }

                SaveCurrentStat(di.FullName, grpsbyext, subDirCount);
            }
            catch //if cant open folder
            {
                RiseNewEvent("Directory " + di.FullName + "\ncould not be accessed!", Events.Unaccess);
                return;
            }

            foreach (DirectoryInfo d in di.GetDirectories())
            {
                ListAllFiles(d, searchPattern);
            }
        }

        private void SaveCurrentStat(string dirFullName, Dictionary<string, ExtGrp> grpsbyext, int subDirCount)
        {
            sb.AppendLine(dirFullName);
            sb.AppendLine("Number of subdirectories: " + subDirCount);
            sb.AppendLine("Statistic for groups of files:");

            foreach (KeyValuePair<string, ExtGrp> kvp in grpsbyext)
            {
                if (stopped)
                {
                    RiseNewEvent("Thread stopped while creating report", Events.Stop);
                    Terminate();
                    sb.Clear();
                    return;
                }

                sb.AppendLine("\tExtension: " + kvp.Key);
                sb.AppendLine("\tNumber of files: " + kvp.Value.Count);
                sb.AppendLine("\tTotal size of group in bytes: " + kvp.Value.TotalLength);
                sb.AppendLine("\tAverage size of group in bytes: " + kvp.Value.AvgLength);
                sb.AppendLine();
            }
            sb.AppendLine();
        }

        private void OnUnaccessiblePathReached(FSAnalysisEventArgs e)
        {
            EventHandler<FSAnalysisEventArgs> handler = UnaccessiblePathReached;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnScanCompleted(FSAnalysisEventArgs e)
        {
            EventHandler<FSAnalysisEventArgs> handler = ScanCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnGenerateReportStarted(FSAnalysisEventArgs e)
        {
            EventHandler<FSAnalysisEventArgs> handler = GenerateReportStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnGenerateReportFinished(FSAnalysisEventArgs e)
        {
            EventHandler<FSAnalysisEventArgs> handler = GenerateReportFinished;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnScanNewDirStarted(FSAnalysisEventArgs e)
        {
            EventHandler<FSAnalysisEventArgs> handler = ScanNewDirStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnExecutionStopped(FSAnalysisEventArgs e)
        {
            EventHandler<FSAnalysisEventArgs> handler = ExecutionStopped;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void RiseNewEvent(string msg, Events ev)
        {
            fsa.Message = msg;
            switch (ev)
            {
                case Events.Unaccess:
                    OnUnaccessiblePathReached(fsa);
                    break;
                case Events.ScanComplete:
                    OnScanCompleted(fsa);
                    break;
                case Events.GenReportStart:
                    OnGenerateReportStarted(fsa);
                    break;
                case Events.GenReportFinish:
                    OnGenerateReportFinished(fsa);
                    break;
                case Events.ScanNewDir:
                    OnScanNewDirStarted(fsa);
                    break;
                case Events.Stop:
                    OnExecutionStopped(fsa);
                    break;
                default:
                    break;
            }
        }
    }
}
