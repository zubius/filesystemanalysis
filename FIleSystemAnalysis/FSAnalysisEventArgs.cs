﻿using System;

namespace FileSystemAnalysis
{
    public class FSAnalysisEventArgs : EventArgs
    {
        public string Message { get; set; }
    }
}
