﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace FileSystemAnalysis
{
    public class FileSystemInfo
    {
        private static int ID = 0;
        private static Dictionary<int, Thread> threads = new Dictionary<int, Thread>();
        private static Dictionary<int, FileSystemAnalysis> flags = new Dictionary<int, FileSystemAnalysis>();

        public static int Run(string path)
        {
            int id = ID;
            Thread th = new Thread(() => StartNew(path, id));
            th.Start();
            threads.Add(id, th);
            ID++;
            return id;
        }

        public static void Stop(int id)
        {
            flags[id].Stopped = true;
        }

        public static void ForceStop(int id)
        {
            threads[id].Abort();
        }

        internal static void RemoveThread(int key)
        {
            threads.Remove(key);
            flags.Remove(key);
            ID--;
        }

        static void StartNew(string path, int id)
        {
            try
            {
                FileSystemAnalysis fs = new FileSystemAnalysis(@path, id);
                flags.Add(id, fs);
                fs.Start();
            }
            catch (ThreadAbortException tae)
            {
                FSAnalysisEventArgs fsa = new FSAnalysisEventArgs();
                fsa.Message = "Thread " + id + " was aborted";
                OnThreadAborted(fsa);
            }
        }

        public static event EventHandler<FSAnalysisEventArgs> UnaccessiblePathReached
        {
            add { FileSystemAnalysis.UnaccessiblePathReached += value; }
            remove { FileSystemAnalysis.UnaccessiblePathReached -= value; }
        }

        public static event EventHandler<FSAnalysisEventArgs> ScanNewDirStarted
        {
            add { FileSystemAnalysis.ScanNewDirStarted += value; }
            remove { FileSystemAnalysis.ScanNewDirStarted -= value; }
        }

        public static event EventHandler<FSAnalysisEventArgs> ScanCompleted
        {
            add { FileSystemAnalysis.ScanCompleted += value; }
            remove { FileSystemAnalysis.ScanCompleted -= value; }
        }

        public static event EventHandler<FSAnalysisEventArgs> GenerateReportStarted
        {
            add { FileSystemAnalysis.GenerateReportStarted += value; }
            remove { FileSystemAnalysis.GenerateReportStarted -= value; }
        }

        public static event EventHandler<FSAnalysisEventArgs> GenerateReportFinished
        {
            add { FileSystemAnalysis.GenerateReportFinished += value; }
            remove { FileSystemAnalysis.GenerateReportFinished -= value; }
        }

        public static event EventHandler<FSAnalysisEventArgs> ExecutionStopped
        {
            add { FileSystemAnalysis.ExecutionStopped += value; }
            remove { FileSystemAnalysis.ExecutionStopped -= value; }
        }

        public static event EventHandler<FSAnalysisEventArgs> ThreadAborted;

        private static void OnThreadAborted(FSAnalysisEventArgs e)
        {
            EventHandler<FSAnalysisEventArgs> handler = ThreadAborted;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
